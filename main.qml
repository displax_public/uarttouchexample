import QtQuick 2.14
import QtQuick.Window 2.14
import QtQuick.Controls 2.14
import QtQuick.Layouts 1.14

Window {
    id: root
    width: 640
    height: 480
    visible: true
    title: qsTr("Displax Uart Touch")

    onClosing: { // If communication is open, close it.
        if(sensorView.visible)
            uartTouch.stop();
    }

    ColumnLayout {
        anchors.fill: parent
        RowLayout{
            Layout.margins: 10
            spacing: 10
            ComboBox { // Show serial ports available.
                id: serial_combo_box
                width: 200
                model: uartTouch.serialPortsName
                enabled: !sensorView.visible
            }

            Button { // Open/Close communication. On opening, port name and 115200 baudrate is set.
                text: sensorView.visible ? "close" : "open"
                onClicked: {
                    if(sensorView.visible === true) {
                        uartTouch.stop();
                        sensorView.visible = false;
                    } else if (uartTouch.start(serial_combo_box.currentText, "115200")) {
                        sensorView.visible = true;
                    }
                }
            }
        }

        Rectangle { // Default view without communication.
            color: "darkgrey"
            Layout.fillWidth: true
            Layout.fillHeight: true
            visible: !sensorView.visible
        }

        Rectangle { // Default view with communication.
            id: sensorView
            Layout.fillWidth: true
            Layout.fillHeight: true
            visible: false
            color: "black"

            // Each sensor type has own frame size. Here we calculate factor between frame size
            // and screen size.
            property real frame_factor_width: width / uartTouch.frameSize.width
            property real frame_factor_height: height / uartTouch.frameSize.height
            property var colors: ["orange", "red", "blue", "green", "brown", "blueviolet"]

            Repeater{
                model: uartTouch.touches
                Rectangle {
                    // Draw a circle with diameter from pressure
                    width: modelData.wPressure / 16
                    height: modelData.wPressure / 16
                    color: sensorView.colors[index % 6] // Set different color for each touch.
                    radius: modelData.wPressure / 32
                    // Adjust touch position on screen.
                    x: modelData.wXData * sensorView.frame_factor_width
                    y: modelData.wYData * sensorView.frame_factor_height
                }
            }
        }
    }
}
