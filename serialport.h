#ifndef SERIALPORT_H
#define SERIALPORT_H

#include <QByteArray>
#include <QObject>
#include <QtSerialPort/QSerialPort>
#include <QtSerialPort/QSerialPortInfo>

/*
 * This class simplify and buffer the data received from serial port (i.e. Uart)
 *
 * QSerialPort will notify pieces of information. Therefore, the reports can be discontinuous.
 * So, this class will store received data, and you can check data (receivedData), parse and remove
 * parsed data (consumeReceivedData).
 */

class SerialPort: public QObject
{
    Q_OBJECT

public:
    SerialPort();

    const QByteArray& receivedData() const;
    void consumeReceivedData(int size);

    QStringList getListOfAvailableSerialPorts() const;

signals:
    void readyReceivedData();

public slots:
    bool open(const QString& port, const QString& baudrate);
    void close();

    void sendData(const QByteArray& data);

private slots:
    void procReceivedData();
    void errorOccurred(QSerialPort::SerialPortError error);

private:
    QSerialPort m_serial;
    QByteArray m_buffer;
};

#endif // SERIALPORT_H
