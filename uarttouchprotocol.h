#ifndef UARTTOUCHPROTOCOL
#define UARTTOUCHPROTOCOL

#include <QByteArray>
#include <QJsonArray>
#include <QSize>

/*
 * This class parse Uart reports from Displax Uart Protocol
 * Parse, convert and notify handler with new data.
 * It can, also, generate reports to send to firmware through Uart.
 */

class UTPDataView;

class UartTouchProtocol
{
public:
    class Handler
    {
    public:
        virtual void handleStarted() = 0; // received confirmation from start
        virtual void handleStopped() = 0; // received confirmation from stop
        virtual void handleReseted() = 0; // received confirmation from reset
        virtual void handleTouches(const QJsonArray& touches) = 0; // received new touches
        // received device hid descriptor
        virtual void handleDeviceReportDescriptor(const QByteArray& desc) = 0;
        // received touch hid descriptor
        virtual void handleTouchesReportDescriptor(const QByteArray& desc) = 0;
        virtual void handleFrameSize(const QSize& size) = 0;
    };

    UartTouchProtocol();

    int parse(const QByteArray& d, Handler* h);

    QByteArray startReport() const;            // create report to start emiting touches reports
    QByteArray stopReport() const;             // create report to stop emiting touches reports
    QByteArray resetReport() const;            // reset Uart communication in firmware
    QByteArray frameSizeReport() const;        // create report to retrieve frame size
    QByteArray deviceReportDescriptor() const; // create report to retrieve device hid descriptor
    QByteArray touchReportDesciptor() const;   // create report to retrieve touch hid descriptor

private:
    bool parseDeviceDescriptor(UTPDataView* v, Handler* h);
    bool parseTouchesDescriptor(UTPDataView* v, Handler* h);
    bool parseFrameSize(UTPDataView* v, Handler* h);
    bool parseTouches(UTPDataView* v, Handler* h);
    bool parseStart(UTPDataView* v, Handler* h);
    bool parseStop(UTPDataView* v, Handler* h);
    bool parseReset(UTPDataView* v, Handler* h);
};

#endif // UARTTOUCHPROTOCOL
