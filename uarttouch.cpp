#include "uarttouch.h"
#include <QTimer>

UartTouch::UartTouch(QObject* parent)
    : QObject(parent)
{
    connect(&m_uart, SIGNAL(readyReceivedData()), this, SLOT(receivedData()));
}

QJsonArray UartTouch::touches() const
{
    return m_touches;
}

QSize UartTouch::frameSize() const
{
    return m_frame_size;
}

QStringList UartTouch::serialPortsName() const
{
    return m_uart.getListOfAvailableSerialPorts();
}

/* Starting is a process divided in 4 steps:
 * 1) open serial port
 * 2) reset device
 * 3) get frame size
 * 4) send start command to retrieve touches
 */
bool UartTouch::start(const QString& port, const QString& baudrate)
{
    if(!m_uart.open(port, baudrate))
        return false;

    // send reset
    m_uart.sendData(m_protocol.resetReport());
    // now wait for reset feedback
    return true;
}

/* Stopping device is a process divided in 2 steps
 * 1) send stop command to cease touches reports
 * 2) close serial port
 * optionaly you can send a reset command before closing.
 */
void UartTouch::stop()
{
    m_uart.sendData(m_protocol.stopReport());
    // wait for stop feedback
}

// Method called when open process is terminated.
void UartTouch::handleStarted()
{
    qInfo() << "Device opened";
}

// Method called after send a stop report. Now close serial connection.
void UartTouch::handleStopped()
{
    m_uart.close();
    m_touches = QJsonArray();
}

// Method called after serial connection reset in firmware. Now get frame size
void UartTouch::handleReseted()
{
    m_uart.sendData(m_protocol.frameSizeReport());
}

// Method called to update touches
void UartTouch::handleTouches(const QJsonArray& touches)
{
    m_touches = touches;
    emit touchesChanged();
}

// Method called with device descriptor
void UartTouch::handleDeviceReportDescriptor(const QByteArray& desc)
{
    Q_UNUSED(desc);
}

// Method called with touch descriptor
void UartTouch::handleTouchesReportDescriptor(const QByteArray& desc)
{
    Q_UNUSED(desc);
}

// Method called after get frame size. Now send start touch report
void UartTouch::handleFrameSize(const QSize& size)
{
    m_uart.sendData(m_protocol.startReport());
    if(m_frame_size == size)
        return;

    m_frame_size = size;
    emit frameSizeChanged();
}

void UartTouch::receivedData()
{
    m_uart.consumeReceivedData(m_protocol.parse(m_uart.receivedData(), this));
}
