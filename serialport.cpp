#include "serialport.h"
#include <QDebug>

SerialPort::SerialPort() {}

QStringList SerialPort::getListOfAvailableSerialPorts() const
{
    QStringList port_list;
    auto list = QSerialPortInfo::availablePorts();

    for(int i = 0; i < list.size(); i++)
        port_list.append(list.at(i).portName());

    return port_list;
}

// Open connection
bool SerialPort::open(const QString& port, const QString& baud)
{
    m_serial.setPortName("/dev/" + port);
    m_serial.setBaudRate(baud.toInt());
    m_serial.setDataBits(QSerialPort::Data8);
    m_serial.setParity(QSerialPort::NoParity);
    m_serial.setStopBits(QSerialPort::OneStop);
    m_serial.setFlowControl(QSerialPort::NoFlowControl);

    m_serial.setReadBufferSize(1);
    m_serial.clearError();
    connect(&m_serial, SIGNAL(readyRead()), this, SLOT(procReceivedData()));
    connect(&m_serial, SIGNAL(errorOccurred(QSerialPort::SerialPortError)), this,
            SLOT(errorOccurred(QSerialPort::SerialPortError)));

    return m_serial.open(QIODevice::ReadWrite);
}

// Close connection
void SerialPort::close()
{
    m_serial.close();
}

// Send data to firmware through serial port (Uart)
void SerialPort::sendData(const QByteArray& data)
{
    if(data.isEmpty())
        return;

    m_serial.write(data);
}

void SerialPort::procReceivedData()
{
    if(m_serial.bytesAvailable() > 0)
    {
        // When data was received, emit a signal for the data be processed in the uart hid protocol class
        m_buffer.append(m_serial.readAll());
        emit readyReceivedData();
    }
}

void SerialPort::errorOccurred(QSerialPort::SerialPortError error)
{
    qWarning() << error << m_serial.errorString();
}

const QByteArray& SerialPort::receivedData() const
{
    return m_buffer;
}

// Remove left consumed data
void SerialPort::consumeReceivedData(int size)
{
    if(size <= 0)
        return;

    if(size >= m_buffer.length())
        m_buffer.clear();
    else
        m_buffer = m_buffer.right(m_buffer.length() - size);
}
