#include "uarttouchprotocol.h"
#include <QDebug>
#include <QJsonObject>

#pragma pack(push, 1)
struct UartTouch
{
    quint8 bStatus;
    quint8 contactId;
    quint16 wXData;
    quint16 wYData;
    quint8 bWidth;
    quint8 bHeight;
    quint16 wPressure;
};

struct UartTouchesReport
{
    quint8 reportID;
    UartTouch touches[6];
    quint8 actualCount;
    quint16 scanTime;
};

#pragma pack(pop)

// Helper class
class UTPDataView
{
public:
    inline UTPDataView(const QByteArray& data)
        : m_data(data)
        , m_pos(0)
    {}

    template<typename T>
    inline T readValue() const
    {
        T v;
        memcpy(&v, m_data.constData() + m_pos, sizeof(T));
        return v;
    }

    template<typename T>
    inline T takeValue()
    {
        T v = readValue<T>();
        m_pos += sizeof(T);
        return v;
    }

    inline QByteArray takeData(int size)
    {
        QByteArray v(m_data.constData(), size);
        m_pos += size;
        return v;
    }

    inline int length() const { return m_data.length() - m_pos; }

    quint32 crc32(quint32 data_size)
    {
        const quint32* data = reinterpret_cast<const uint32_t*>(m_data.constData());
        const quint32 crc_table[16] = {0x00000000, 0x04C11DB7, 0x09823B6E, 0x0D4326D9,
                                       0x130476DC, 0x17C56B6B, 0x1A864DB2, 0x1E475005,
                                       0x2608EDB8, 0x22C9F00F, 0x2F8AD6D6, 0x2B4BCB61,
                                       0x350C9B64, 0x31CD86D3, 0x3C8EA00A, 0x384FBDBD};
        quint32 crc = 0xFFFFFFFF;
        const quint32* data_end = data + data_size / 4;
        while(data < data_end)
        {
            crc = crc ^ (*data);
            crc = (crc << 4) ^ crc_table[crc >> 28];
            crc = (crc << 4) ^ crc_table[crc >> 28];
            crc = (crc << 4) ^ crc_table[crc >> 28];
            crc = (crc << 4) ^ crc_table[crc >> 28];
            crc = (crc << 4) ^ crc_table[crc >> 28];
            crc = (crc << 4) ^ crc_table[crc >> 28];
            crc = (crc << 4) ^ crc_table[crc >> 28];
            crc = (crc << 4) ^ crc_table[crc >> 28];
            data++;
        }
        return crc;
    }

private:
    const QByteArray m_data;
    int m_pos;
};

UartTouchProtocol::UartTouchProtocol() {}

// In this method we will parse data received.
// Return size of data parsed. On error detection discard all data, returning data size.
int UartTouchProtocol::parse(const QByteArray& d, Handler* h)
{
    UTPDataView view(d);
    bool f = true;

    while(view.length() >= 2 && f)
        switch(view.readValue<quint16>())
        {
            case 0x01: f = parseDeviceDescriptor(&view, h); break;  // Device HID Descriptor
            case 0x02: f = parseTouchesDescriptor(&view, h); break; // HID Touch Report Descriptor
            case 0x03: f = parseFrameSize(&view, h); break; // Report touch controller frame size
            case 0x04: f = parseTouches(&view, h); break;   // Touch report
            case 0x05: f = parseStart(&view, h); break; // Confirmation of start reporting over UART
            case 0x06: f = parseStop(&view, h); break;  // Confirmation of stop reporting over UART
            case 0x226E: f = parseReset(&view, h); break; // Confirmation of reset
            default: return d.size();                     // Error detection, clear buffer
        }

    return d.length() - view.length();
}

bool UartTouchProtocol::parseDeviceDescriptor(UTPDataView* v, Handler* h)
{
    if(v->length() < 32)
        return false;

    v->takeValue<quint16>(); // take code
    h->handleDeviceReportDescriptor(v->takeData(30));
    return true;
}

bool UartTouchProtocol::parseTouchesDescriptor(UTPDataView* v, Handler* h)
{
    if(v->length() < 708)
        return false;

    v->takeValue<quint16>(); // take code
    h->handleTouchesReportDescriptor(v->takeData(708));
    return true;
}

bool UartTouchProtocol::parseFrameSize(UTPDataView* v, Handler* h)
{
    if(v->length() < 6)
        return false;

    v->takeValue<quint16>();              // take code
    quint16 _w = v->takeValue<quint16>(); // take width
    quint16 _h = v->takeValue<quint16>(); // take height

    h->handleFrameSize(QSize(_w, _h));
    return true;
}

bool UartTouchProtocol::parseTouches(UTPDataView* v, Handler* h)
{
    if(v->length() < int(sizeof(UartTouchesReport) + 8)) // size: 72
        return false;

    quint32 crc_calculated = v->crc32(sizeof(UartTouchesReport) + 4);       // calculate crc
    quint16 code_id = v->takeValue<quint16>();                              // take code
    quint16 report_size = v->takeValue<quint16>();                          // take report size
    UartTouchesReport hid_touch_report = v->takeValue<UartTouchesReport>(); // take touches report
    quint32 crc_received = v->takeValue<quint32>(); // take CRC calculated by firmware

    Q_UNUSED(code_id);
    Q_UNUSED(report_size);

    // check if crc match
    if(crc_calculated != crc_received)
    {
        qDebug() << "Error in CRC, dropping messages!";
        qDebug("Got: 0x%.8x, Calc: 0x%.8x", crc_received, crc_calculated);
        v->takeData(v->length());
        return false;
    }

    // convert to json
    QJsonArray touches;
    for(int i = 0; i < hid_touch_report.actualCount && i < 6; ++i)
    {
        // status equal to 7 is ok, otherwise discard
        if(hid_touch_report.touches[i].bStatus != 7)
            continue;

        QJsonObject obj;
        obj.insert("contactId", hid_touch_report.touches[i].contactId);
        obj.insert("bStatus", hid_touch_report.touches[i].bStatus);
        obj.insert("wXData", hid_touch_report.touches[i].wXData);
        obj.insert("wYData", hid_touch_report.touches[i].wYData);
        obj.insert("wPressure", hid_touch_report.touches[i].wPressure);
        touches.append(obj);
    }

    h->handleTouches(touches);
    return true;
}

bool UartTouchProtocol::parseStart(UTPDataView* v, Handler* h)
{
    if(v->length() < 2)
        return false;

    v->takeValue<quint16>(); // take code
    h->handleStarted();
    return true;
}

bool UartTouchProtocol::parseStop(UTPDataView* v, Handler* h)
{
    if(v->length() < 2)
        return false;

    v->takeValue<quint16>(); // take code
    h->handleStopped();
    return true;
}

bool UartTouchProtocol::parseReset(UTPDataView* v, Handler* h)
{
    if(v->length() < 2)
        return false;

    v->takeValue<quint16>(); // take code
    h->handleReseted();
    return true;
}

QByteArray UartTouchProtocol::startReport() const
{
    const char r[] = {0x5, 0x0};
    return QByteArray(r, sizeof(r));
}

QByteArray UartTouchProtocol::stopReport() const
{
    const char r[] = {0x6, 0x0};
    return QByteArray(r, sizeof(r));
}

QByteArray UartTouchProtocol::resetReport() const
{
    const char r[] = {0x0, 0x0};
    return QByteArray(r, sizeof(r));
}

QByteArray UartTouchProtocol::frameSizeReport() const
{
    const char r[] = {0x3, 0x0};
    return QByteArray(r, sizeof(r));
}

QByteArray UartTouchProtocol::deviceReportDescriptor() const
{
    const char r[] = {0x1, 0x0};
    return QByteArray(r, sizeof(r));
}

QByteArray UartTouchProtocol::touchReportDesciptor() const
{
    const char r[] = {0x2, 0x0};
    return QByteArray(r, sizeof(r));
}
