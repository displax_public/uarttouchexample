#ifndef UARTTOUCH_H
#define UARTTOUCH_H

#include <serialport.h>
#include <uarttouchprotocol.h>

/*
 * UartTouch abstracts SerialPort and UartTouchProtocol, to provide a simple interface to qml.
 * This class provide all service ports names available.
 * Allow to connect (start) to one serial port, and do all steps necessary to start.
 * On connect, will emit touch events.
 */

class UartTouch: public QObject, private UartTouchProtocol::Handler
{
    Q_OBJECT
    Q_PROPERTY(QJsonArray touches READ touches NOTIFY touchesChanged)
    Q_PROPERTY(QSize frameSize READ frameSize NOTIFY frameSizeChanged)
    Q_PROPERTY(QStringList serialPortsName READ serialPortsName NOTIFY serialPortsNameChanged)

public:
    explicit UartTouch(QObject* parent = nullptr);

    QJsonArray touches() const;
    QSize frameSize() const;
    QStringList serialPortsName() const;

signals:
    void touchesChanged();
    void frameSizeChanged();
    void serialPortsNameChanged();

public slots:
    bool start(const QString& port, const QString& baudrate); // start retrieve touches
    void stop();                                              // stop retrieve touches

private:
    void handleStarted() override;
    void handleStopped() override;
    void handleReseted() override;
    void handleTouches(const QJsonArray& touches) override;
    void handleDeviceReportDescriptor(const QByteArray& desc) override;
    void handleTouchesReportDescriptor(const QByteArray& desc) override;
    void handleFrameSize(const QSize& size) override;

private slots:
    void receivedData();

private:
    SerialPort m_uart;
    UartTouchProtocol m_protocol;
    QJsonArray m_touches;
    QSize m_frame_size;
};

#endif // UARTTOUCH_H
